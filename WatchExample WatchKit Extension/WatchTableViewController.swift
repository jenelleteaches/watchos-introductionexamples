//
//  WatchTableViewController.swift
//  WatchExample WatchKit Extension
//
//  Created by parrot on 2018-12-03.
//  Copyright © 2018 room1. All rights reserved.
//

import WatchKit
import Foundation

class WatchTableViewController: WKInterfaceController {
    
    // 1. define your data source for the table
    var countries = ["Belgium", "USA", "UK", "India", "China", "Australia"]
    
    @IBOutlet var tableView: WKInterfaceTable!
 
    
    
    override func awake(withContext context: Any?) {
        tableView.setNumberOfRows(countries.count, withRowType: "CountryRowControllerIdentifier")
        
        
        for i in 0..<countries.count {
            let row = tableView.rowController(at: i) as! CountryRowController
            row.countryLabel.setText(countries[i])
        }
        
    }
    
}
