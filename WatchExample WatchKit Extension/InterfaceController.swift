//
//  InterfaceController.swift
//  WatchExample WatchKit Extension
//
//  Created by parrot on 2018-12-03.
//  Copyright © 2018 room1. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    
    @IBOutlet var titleLabel: WKInterfaceLabel!
    @IBOutlet var countLabel: WKInterfaceLabel!
    
    private var count = 0
    
    var timer = Timer()
    
    
    @objc func fireTimer() {
        print("firing timer")
        count = count + 1
        countLabel.setText("\(count)")
    }
    
    @IBAction func resetButtonPressed() {
        titleLabel.setText("Hello world!")
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: true)
    }
    
    
    @IBAction func stopButtonPressed() {
        timer.invalidate()
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
}
